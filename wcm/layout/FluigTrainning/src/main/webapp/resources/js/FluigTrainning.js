var filtraPoa = DatasetFactory.createConstraint("unidade", "Porto Alegre", "Porto Alegre", ConstraintType.MUST);
var estado = DatasetFactory.createConstraint("status", "Ativo", "Ativo", ConstraintType.MUST);
var treinamentosPoa = DatasetFactoryAuth.getDataset('RegistroCadastroTrainning', null, new Array(filtraPoa, estado), null).values;
var filtraCaxias = DatasetFactory.createConstraint("unidade", "Caxias do Sul", "Caxias do Sul", ConstraintType.MUST);
var treinamentosCaxias = DatasetFactoryAuth.getDataset('RegistroCadastroTrainning', null, new Array(filtraCaxias, estado), null).values;

var that = this;

function valida(dataTreinamento){

  var input = dataTreinamento.split('/');
  var hoje = moment().format('DD/MM/YYYY').split('/');
  var verdade = true;

  input[0] = parseInt(input[0]);
  input[1] = parseInt(input[1]);
  input[2] = parseInt(input[2]);

  hoje[0] = parseInt(hoje[0]);
  hoje[1] = parseInt(hoje[1]);
  hoje[2] = parseInt(hoje[2]);


  if (input[2] < hoje[2]) {
      verdade = false;

  }

  if (input[1] < hoje[1]) {
      verdade = false;
  }

  if (input[1] == hoje[1]) {

      if (input[0] < hoje[0]) {
          verdade = false;
      }

  }

  if (verdade == true) {
      validado = true;
  }
else {
    validado = false;
}
return validado;
};

$(document).ready(function () {

  naData=false;
 
  $.each(that.treinamentosPoa, function (key, value) {

    if(valida(that.treinamentosPoa[key].dataTreinamento)==true){

    naData=true;
    $("#aqui").append("<div class='treinaPoa col-md-3 col-sm-6 col-xs-12 col-12'><div style='margin-bottom:60px;' class='card'><div id='oi' class='card-body'><input type='hidden' id='idTreinamento' value='" + that.treinamentosPoa[key].documentid + "'><input type='hidden' id='instrutor' value='" + that.treinamentosPoa[key].instrutor + "'><h5 class='card-title' style='background-color:#4a4a4a;'>" + that.treinamentosPoa[key].titulo + "</h5><p class='card-text' style='height:150px;'>" + that.treinamentosPoa[key].descricaoResumida + "</p><input type='hidden' id='desCompleta' value='" + that.treinamentosPoa[key].descricaoCompleta + "'><input type='hidden' id='preco' value='" + that.treinamentosPoa[key].valor + "'><input type='hidden' id='local' value='" + that.treinamentosPoa[key].unidade + "'><input type='hidden' id='horario' value='" + that.treinamentosPoa[key].horario + "'><button style='font-weight: bold; position:absolute; bottom: -20px; right: 0; background-color: #ffb34c;  border-color: #ffb34c;' class='saiba btn btn-primary' </button><a>SAIBA MAIS!</button><p class='card-text'><small class='text-muted'>Data:</small><small id='data' class='text-muted'>" + that.treinamentosPoa[key].dataTreinamento + "</small></p></div></div></div>");
    
  }

  });

  if(naData==false){
    $("#aqui").append("<h1 class='treinaPoa'>Nenhum Treinamento Disponível</h1>");
  }

  populaModal();

  
});

$(".btn-group>.btn").click(function () {
  $(".btn-group>.btn").removeClass("active");
  $(this).addClass("active");
});

mostraPoa();
mostraCaxias();


function mostraPoa() {
  $('#poa').click(function () {
    naData=false;
    $(".treinaPoa").remove();
    $(".treinaCaxias").remove();
    $.each(that.treinamentosPoa, function (key) {
      if(valida(that.treinamentosPoa[key].dataTreinamento)==true){
        naData=true;
      $("#aqui").append("<div class='treinaPoa col-md-3 col-sm-6 col-xs-12 col-12'><div style='margin-bottom:60px;' class='card'><div id='oi' class='card-body'><input type='hidden' id='idTreinamento' value='" + that.treinamentosPoa[key].documentid + "'><input type='hidden' id='instrutor' value='" + that.treinamentosPoa[key].instrutor + "'><h5 class='card-title' style='background-color:#4a4a4a;'>" + that.treinamentosPoa[key].titulo + "</h5><p class='card-text' style='height:150px;'>" + that.treinamentosPoa[key].descricaoResumida + "</p><input type='hidden' id='desCompleta' value='" + that.treinamentosPoa[key].descricaoCompleta + "'><input type='hidden' id='preco' value='" + that.treinamentosPoa[key].valor + "'><input type='hidden' id='local' value='" + that.treinamentosPoa[key].unidade + "'><input type='hidden' id='horario' value='" + that.treinamentosPoa[key].horario + "'><button style='font-weight: bold; position:absolute; bottom: -20px; right: 0; background-color: #ffb34c;  border-color: #ffb34c;' class='saiba btn btn-primary' </button><a>SAIBA MAIS!</button><p class='card-text'><small class='text-muted'>Data:</small><small id='data' class='text-muted'>" + that.treinamentosPoa[key].dataTreinamento + "</small></p></div></div></div>");
      }

      });
      if(naData==false){
        $("#aqui").append("<h1 class='treinaPoa'>Nenhum Treinamento Disponível</h1>");
      }
      populaModal();
  });
}

function mostraCaxias() {
  $('#caxias').click(function () {
    naData=false;
    $(".treinaPoa").remove();
    $(".treinaCaxias").remove();
    $.each(that.treinamentosCaxias, function (key, value) {
      if(valida(that.treinamentosCaxias[key].dataTreinamento)==true){
        naData=true;
      $("#aqui").append("<div class='treinaCaxias col-md-3 col-sm-6 col-xs-12 col-12'><div style='margin-bottom:60px;' class='card'><div id='oi' class='card-body'><input type='hidden' id='idTreinamento' value='" + that.treinamentosPoa[key].documentid + "'><input type='hidden' id='instrutor' value='" + that.treinamentosCaxias[key].instrutor + "'><h5 class='card-title' style='background-color:#4a4a4a;'>" + that.treinamentosCaxias[key].titulo + "</h5><p class='card-text' style='height:150px;'>" + that.treinamentosCaxias[key].descricaoResumida + "</p><input type='hidden' id='desCompleta' value='" + that.treinamentosCaxias[key].descricaoCompleta + "'><input type='hidden' id='preco' value='" + that.treinamentosCaxias[key].valor + "'><input type='hidden' id='local' value='" + that.treinamentosCaxias[key].unidade + "'><input type='hidden' id='horario' value='" + that.treinamentosCaxias[key].horario + "'><button style='font-weight: bold; position:absolute; bottom: -20px; right: 0; background-color: #ffb34c;  border-color: #ffb34c;' class='saiba btn btn-primary' </button><a>SAIBA MAIS!</button><p class='card-text'><small class='text-muted'>Data:</small><small id='data' class='text-muted'>" + that.treinamentosCaxias[key].dataTreinamento + "</small></p></div></div></div>");
     }
     });
     if(naData==false){
      $("#aqui").append("<h1 class='treinaCaxias'>Nenhum Treinamento Disponível</h1>");
    }
 populaModal();
  });
}

function populaModal() {
  $('.loader').hide();
  $('.saiba').click(function () {
    console.log("passou");
    $("#descricaoModal").html('');
    $("#cnpj").hide();
    $("#confirmarInsc").remove();
    $("#cadastro").hide();
    $("#dadosTreinamento").show();
    $('#continuar').show();
    $("#tituloModal").text($(this).parent().find('.card-title').html());
    $("#descricaoModal").append($(this).parent().find('#desCompleta').val());
    $("#unidadeModal").text($(this).parent().find('#local').val());
    $("#dataModal").text($(this).parent().find('#data').text());
    $("#horarioModal").text($(this).parent().find('#horario').val());
    $("#instrutorModal").text($(this).parent().find('#instrutor').val());
    $("#precoModal").text($(this).parent().find('#preco').val());
    $("#idTreinamentoModal").val($(this).parent().find('#idTreinamento').val());
    $("#myModal").modal("show");
  });
};

$('#continuar').click(function () {
  $('#continuar').hide();
  $(".modal-dialog").css("width", "798");
  $("#dadosTreinamento").hide();
  $("#cnpj").show();
});

$('#validar').click(function () {

  var cnpjInserido = $('#insiraCnpj').val();
  var cnpjLimpado = cnpjInserido.replace(/\D/g, '');
  var colunasDsGetAllClients = new Array('A1_CGC', 'A1_NOME');
  var cnpj = DatasetFactory.createConstraint("A1_CGC", cnpjLimpado, cnpjLimpado, ConstraintType.MUST);
  var valida = false;

  if (cnpjLimpado != "") {
    var clientes = DatasetFactoryAuth.getDataset('dsGetAllClients', colunasDsGetAllClients, [cnpj], null).values;

    if (clientes.length >= 1) {
      valida = true;
    }
  }

  if (valida == false) {
    alertify.alert('Aviso:', 'CNPJ não encontrado em nossa base de dados, caso queira ser cliente da TOTVS, ou ache que isso foi um erro entre em contato pelo numero (51) 3025-6900').set('movable', false);
  }


  if (valida == true) {

    $("#cnpj").hide();
    $("#cnpj2").val($("#insiraCnpj").val());
    $("#treinamentoEscolhido").val($("#tituloModal").html());
    $("#dataTreinamentoEscolhido").val($("#dataModal").html());
    $("#cadastro").show();
    $(".modal-footer").append("<button type='button' id='confirmarInsc' class='btn btn-primary'>Confirmar Inscrição</button>");
    $('#rg').mask('99.999.999-9'); 
    $('#telefone').mask('(99) 9999-9999'); 
    $('#celular').mask('(99) 99999-9999'); 
    $('#confirmarInsc').click(function () {

      $(function () {

        $('.loader').show();

        setTimeout(function () {      


          
          if ($('#nome').val() == "" || $('#nome').val() == undefined || $('#nome').val() == null ||
            $('#email').val() == "" || $('#email').val() == undefined || $('#email').val() == null ||
            $('#rg').val() == "" || $('#rg').val() == undefined || $('#rg').val() == null ||
            $('#dataNasc').val() == "" || $('#dataNasc').val() == undefined || $('#rg').val() == null ||
            $('#telefone').val() == "" || $('#telefone').val() == undefined || $('#telefone').val() == null ||
            $('#celular').val() == "" || $('#celular').val() == undefined || $('#celular').val() == null
          ) {
            $('.loader').hide();
            alertify.alert('Aviso:', 'Verifique se todos os campos obrigatórios foram preenchidos corretamente!').set('movable', false);
          } else {

            
            DatasetFactoryAuth.postCreateCard(cnpjLimpado);
            DatasetFactoryAuth.postSendEmail($('#email').val(), 'totvsrs-noreply@totvsrs.com.br', 'Inscrição de Treinamento TOTVS', 'templateInscTreinamentos');
            $('.loader').hide();
            $('#myModal').modal('hide');
            FLUIGC.toast({
              title: '',
              message: 'A sua inscrição foi recebida com sucesso! Aguarde email com mais informações',
              type: 'success',
              timeout: 1000
            });
            $('#formModal').trigger("reset");
          }
        }, 500);
      });
    });
  }
});



$('.saiba').click(function () {
  $("#cnpj").hide();
  $("#dadosTreinamento").show();
  $("#tituloModal").text($(this).parent().find('.card-title').html());
  $("#myModal").modal("show");
});



$('#inCompany').click(function () {
  alertify.alert('In Company:', 'Caro cliente, caso tenha interesse em solicitar o treinamento "In Company" (modalidade onde a TOTVS faz o treinamento presencialmente em sua empresa) entre em contato com: (51) 3025-6900 ou envie um email para RH@totvsrs.com.br').set('movable', false);
});


$('#treinalink').click(function () {
  $('#secaoTreinamentos').scrollThere(-$('nav').outerHeight());
});

$('#ancora').click(function () {
  $('#secaoTreinamentos').scrollThere(-$('nav').outerHeight());
});
