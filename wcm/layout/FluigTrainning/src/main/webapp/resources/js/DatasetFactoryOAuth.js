DatasetFactoryAuth = {
    // Chamadas REST API
 getAvailableDatasets: function() {
     var APIData = {
         url: '/api/public/ecm/dataset/availableDatasets',
         method: 'GET'
     }
     
     // Faz chamada AJAX configurada
     this.restAPICall(APIData);
 },

 getDatasetStructure(datasetId) {
     var APIData = {
         url: '/api/public/ecm/dataset/datasetStructure/' + datasetId,
         method: 'GET'
     }
     
     // Faz chamada AJAX configurada
     this.restAPICall(APIData);
 },

 getDataset:function(nameDataset,fields = null, constraint = null, ordem = null){
     var cont = {
         constraints: constraint,
         fields: fields,
         name: nameDataset,
         order: ordem
     }
     var APIData = {
         url: '/api/public/ecm/dataset/datasets/',
         method: 'POST',
         content: cont
     };

     var ds = this.restAPICall(APIData);
     return ds
 },

 getListDocument: function(folderId){
    var APIData = {
        url: '/api/public/ecm/document/listDocument/' + folderId,
        method: 'GET'
    };
    var ds = this.restAPICall(APIData);

    return ds;
},


postSendEmail: function (to, from, subject, templateId) {
    //body        
    var sendMail = {
        "to": to,
        "from": from,
        "subject": subject,
        "templateId": templateId,
        "dialectId": "pt_BR",

        //binding customização no template
        "param": {
            "nome": $('#nome').val()
        }
    }
    
    var APIData = {
        url: '/api/public/alert/customEmailSender',
        method: 'POST',
        content: sendMail
    };

    var ds = this.restAPICall(APIData);
    return ds
},


postCreateCard: function (cnpjLimpado) {

    
    
    var nome= $('#nome').val();   
    var email = $('#email').val();
    var rg = $('#rg').val();
    var dataNasc = $('#dataNasc').val();
    var sexo = $("input[name='sexo']:checked").val();;
    var telefone = $('#telefone').val();
    var celular =$('#celular').val();
    var cargo =  $('#cargo').val();
    var cnpj = cnpjLimpado;
    var treinamentoEscolhido = $('#idTreinamentoModal').val();
    var dataTreinamentoEscolhido= $('#dataTreinamentoEscolhido').val();

  

    function getIdade() {

        var nascimento = dataNasc;
        nascimento = nascimento.split('-');

        hoje = moment().format('YYYY/MM/DD').split('/');
        b = hoje[0] - nascimento[0];

        if (nascimento[1] > hoje[1]) {
            b--;
        }

        if (nascimento[1] == hoje[1] && nascimento[2] > hoje[2]) {
            b--;
        }
        return b;
    }

    var idade = getIdade();

 
    
    var dados = 
    {
      "parentDocumentId": 66039, 
      "version": 1000,
      
      "formData": [ 
        {
          "name": "nome",
          "value": nome
        },
        {
            "name": "email",
            "value": email
         },
         {
            "name": "rg",
            "value": rg
         },
         {
            "name": "dataNasc",
            "value": idade
         },
         {
            "name": "sexo",
            "value": sexo
         },
         {
            "name": "telefone",
            "value": telefone
         },
         {
            "name": "celular",
            "value": celular
         },
         {
            "name": "cargo",
            "value": cargo
         },
         {
            "name": "cnpj2",
            "value": cnpj
         },
         {
            "name": "treinamentoEscolhido",
            "value": treinamentoEscolhido
         },
         {
            "name": "dataTreinamentoEscolhido",
            "value": dataTreinamentoEscolhido
         },
         {
            "name": "pagamentoInscrito",
            "value": "Não"
         },
    
    
      ]
    };
    
    var APIData = {
        url: '/api/public/2.0/cards/create',
        method: 'POST',
        Accept : "text/html",
        content: dados
    };
   
    var ds = this.restAPICall(APIData);
    return ds
},






 // Funções auxiliares padrões para chamadas REST
 getOAuthConfig: function() {
     var consumerPublic = "GET";
     var consumerSecret = "GET";
     var tokenPublic = "3be98caa-edfb-4f34-bcb4-27944544e6bb";
     var tokenSecret = "7a95f0ae-270f-4ceb-ab3c-41934eec81232b1e9f42-34d7-428e-9ab4-6da386ba528f";
     var _url = "http://fluighml.totvsrs.com.br:8080";

     // Lipa os Cookies
     removeCookies();

     var oauth = OAuth({
         consumer: {
             'public': consumerPublic, 
             'secret': consumerSecret
         },
         signature_method: 'HMAC-SHA1',
         parameter_seperator: ",",
         nonce_length: 6
     }); 

     var token = {
         'public': tokenPublic,
         'secret': tokenSecret
     };

     return {
         oauth: oauth,
         token: token,
         url: _url
     }
 },

//  soapAPICall: function (xmlLocalPath,objetoCamposValorXml) {
//     var request = this.getOAuthConfig();
//     var requestData = {
//         url: xmlLocalPath,        
//         datatype: 'xml',
//         method: 'GET'
//     };
//     console.log(request);
//     var auth = request.oauth.toHeader(request.oauth.authorize(requestData, request.token));
//     console.log(auth);
//     var header = {
//         'Access-Control-Allow-Origin': '*',
//         'Access-Control-Allow-Methods': 'GET',
//         'Access-Control-Allow-Headers': 'datatype',
//         Authorization: auth.Authorization
//     }
//     console.log(objetoCamposValorXml);
//     var _xml;


//     $.ajax({
//             url: requestData.url,
//             type: requestData.method,
//             async: false,
//             datatype: requestData.datatype,
//             headers: header
//         })
//         .done(function (xml) {
//             _xml = $(xml);

//         })
//         .fail(function (data) {
//             console.log(data);
//         });

//         $.each(objetoCamposValorXml, function (key) {
//             _xml.find(objetoCamposValorXml[key].nome).text(objetoCamposValorXml[key].valor);
//           });
      
//           parent.WCMAPI.Create({
//             url: "/webdesk/ECMCardService?wsdl",
//             contentType: "text/xml;charset=utf-8",
//             dataType: requestData.datatype,
//             data: _xml[0],
//             success: function (data) {
//               FLUIGC.toast({
//                 title: '',
//                 message: 'A sua inscrição  foi recebida com sucesso! Aguarde email com mais informações',
//                 type: 'success',
//                 timeout: 2000
//               });
//               console.log(_xml);
//               $('#myModal').modal('hide');
//             }
//           })
     

//     return _xml
//  },
 restAPICall: function(APICall) {
     var request = this.getOAuthConfig();
     var requestData = {
         url: request.url + APICall.url, 
         method: APICall.method,
         content: APICall.method == "POST" ? APICall.content : ''                  
     };

     var auth = request.oauth.toHeader(request.oauth.authorize(requestData, request.token));
     var header = {
         'Access-Control-Allow-Origin'   : '*',
         'Access-Control-Allow-Methods'  : 'GET, POST, PATCH, PUT, DELETE, OPTIONS',
         'Access-Control-Allow-Headers'  : 'Origin, Content-Type, X-Auth-Token',
         Authorization: auth.Authorization
     }
     
     var requestBody = (APICall.method == "POST") ? JSON.stringify(APICall.content) : requestData.ajaxData;
     var resp = '';
     $.ajax({
         url: requestData.url,
         type: requestData.method,
         async: false,
         data: requestBody,
         contentType: "application/json",
         headers: header
     })
     .done(function(data) {       
        resp = data.content
      
     })
     .fail(function(data) {
         console.log(data);
     });
     return resp
 },
 createConstraint:function(nomeCampo, initialValue, finalValue, type) {
     return{
         _field: nomeCampo,
         _finalValue: initialValue,
         _initialValue: finalValue,
         _type: type
     }
 }
}








function removeCookies() {
	document.cookie = "JSESSIONID=;EXPIRES=-1;" + "; path=/";
	document.cookie = "JSESSIONIDSSO=;EXPIRES=-1;" + "; path=/";
	document.cookie = "JSESSIONID=;EXPIRES=-1;" + "; path=/hiring";
	document.cookie = "JSESSIONID=;EXPIRES=-1;" + "; path=/style-guide";
}