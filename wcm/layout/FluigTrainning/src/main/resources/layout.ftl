<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">

  
  <link rel="stylesheet" type="text/css" href="/style-guide/css/fluig-style-guide.min.css">
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css"
    href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" type="text/css" href="/FluigTrainning/resources/css/alertify.css">



  <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
  <script src="/portal/resources/js/mustache/mustache-min.js"></script>
  <script src="/portal/resources/js/mustache/mustache-min.js"></script>
  
  
  <title>Potal de Treinamentos</title>
</head>

<body style="background-color:#e3e6e8">
  <!-- navbar -->
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="nav">
    <div class="container">
      <a class="navbar-brand" href="#">
        <img src="/FluigTrainning/resources/images/logo2.png" alt="">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
        aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li>
            <a href="#" class="linkBar" id="treinalink">TREINAMENTOS</a>
          </li>
          <li>
            <a href="#" class="linkBar" id="inCompany">IN COMPANY</a>
          </li>
          <li>
            <a href="#" class="linkBar"></a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- imagem fundo e titulo -->
  <div class="container" id="containerImagem"></div>


  <div class="imagem">
  
          <div class="frases" style="text-align:center;">  
  
             
              <h1 id="fraseTitulo">Treinamentos Presenciais</h1>
              <hr id="linhaTitulo">
              <h3 id="fraseTitulo2">Aperfeiçoe-se como cliente em nossos serviços e produtos!</h3>
            
          
        </div>
    
  </div>


    

  <div  id="secaoTreinamentos">

  <!-- container -->
  <div class="container" id="cont-cursos">

    <!-- escolhe unidade -->
    <div class="row" id="linhaUnidade">
      <div class="col-lg-2 col-sm-12 col-xs-12 col-12">
        <h5 id="sede">Selecione a sede:</h5>
        <div class="btn-group">
          <button type="button" id="poa" class="active btn btn-primary mx-1">Porto Alegre</button>
          <button type="button" id="caxias" class="btn btn-primary mx-1">Caxias Do Sul</button>
        </div>
      </div>
    </div>

    <div class="row" id="aqui"></div>

    <!-- The Modal -->
    <div class="modal" id="myModal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <!-- Modal Header -->
          <div class="modal-header">
            <h4 class="modal-title">Dados do Treinamento</h4>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
          </div>
          <!-- Modal body -->
          <div class="modal-body">
            <div class="container-fluid">

              <section id="dadosTreinamento">
                <div class="row">
                  <div class="col-lg-12">
                    <h2 id="tituloModal">Titulo</h2>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-6">
                    <h6>Descrição:</h6>
                    <p id="descricaoModal">
                  </div>
                  <div class="col-lg-4 offset-lg-2" style="padding-top:20px;">
                    <div>
                      <span class="spanModal">Local: </span>
                      <span id="unidadeModal">Porto
                        Alegre</span>
                    </div>
                    <div style="padding-top:20px;">
                      <span class="spanModal">Data: </span><span
                        id="dataModal">12/07/2019</span>
                    </div>
                    <div style="padding-top:20px;">
                      <span class="spanModal">Horário: </span><span
                        id="horarioModal">14:00</span>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-6" style="padding-top:20px;">
                  <span class="spanModal">Instrutor: </span><span
                      id="instrutorModal">Carlos</span>
                  </div>
                  <div class="col-lg-4 offset-lg-2" style="padding-top:20px;"><span
                     class="spanModal">Investimento: </span><span style="color:green;">R$
                    </span><span id="precoModal" style="color:green;"></span><span style="color:green;">+juros</span>
                  </div>
                </div>
              </section>


              <section id="cnpj">
                <h5>Insira o CNPJ de sua empresa</h5>
                <div class="row">
                  <div class="col-md-12">
                    <form action="" accept-charset="UTF-8" method="get">
                      <div class="input-group">       
                        <input type="text" name="insiraCnpj" maxlength="18" id="insiraCnpj"
                          placeholder="FORMATOS VÁLIDOS EX: XX.XXX.XXX/XXXX-XX, XXX.XXX.XXX-XX OU APENAS NÚMEROS..." class="form-control mr-2">
                        <span class="input-group-btn">
                          <input type="button" id="validar" name="commit" value="Validar" class="btn btn-primary"
                            data-disable-with="Search">
                        </span>
                      </div>
                    </form>
                  </div>
                </div>
              </section>



              <section id="cadastro">
              <label>* Campos Obrigatórios</label>
                <form id="formModal">
                 	<div class="form-group">
                    <label for="nome">Nome do Participante </label><label style='font-weight:bold;'>&nbsp;*</label>
                    <input type="text" name="nome" class="form-control" id="nome" placeholder="">
                  </div>
                  <div class="form-group">
                    <label for="email">Email </label><label style='font-weight:bold;'>&nbsp;*</label>
                    <input type="text" name="email" class="form-control" id="email" placeholder="">
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-4">
                      <label for="rg">RG </label><label style='font-weight:bold;'>&nbsp;*</label>
                      <input type="text" name="rg" class="form-control" id="rg" placeholder="">
                    </div>
                    <div class="form-group col-md-4">
                      <label for="dataNasc">Data Nasc </label><label style='font-weight:bold;'>&nbsp;*</label>
                      <input type="date" name="dataNasc" class="form-control" id="dataNasc" placeholder="">
                    </div>
                    <div class="form-group col-md-4">
                      <label>Sexo </label><label style='font-weight:bold;'>&nbsp;*</label>
                      <div class="row" id="radioSexo">
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="sexo" value="Masculino" checked>
                        <label class="form-check-label" for="sexo">Masculino</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="sexo"  value="Feminino">
                        <label class="form-check-label" for="sexo">Feminino</label>
                      </div>
                    </div>
                  </div>
                  <div class="loader" id="loader"></div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-4">
                      <label for="telefone">Telefone Comercial</label><label style='font-weight:bold;'>&nbsp;*</label>
                      <input type="text" name="telefone" class="form-control" id="telefone" placeholder="">
                    </div>
                    <div class="form-group col-md-4">
                      <label for="celular">Celular </label><label style='font-weight:bold;'>&nbsp;*</label>
                      <input type="text" name="celular" class="form-control" id="celular" placeholder="">
                    </div>
                    <div class="form-group col-md-4">
                      <label for="cargo">Cargo</label>
                      <input type="text" name="cargo" class="form-control" id="cargo" placeholder="">
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="form-group col-md-4">
                      <label for="cnpj2">CNPJ</label>
                      <input type="text" name="cnpj2" class="form-control" id="cnpj2" placeholder="" readonly>
                    </div>
                    <div class="form-group col-md-4">
                      <label for="treinamentoEscolhido">Treinamento</label>
                      <input type="text" name="treinamentoEscolhido" class="form-control" id="treinamentoEscolhido" placeholder="" readonly>
                    </div>
                    <div class="form-group col-md-4">
                      <label for="dataTreinamentoEscolhido">Data Treinamento</label>
                      <input type="text" class="form-control" name="dataTreinamentoEscolhido" id="dataTreinamentoEscolhido" placeholder="" readonly>
                    </div>
                  
                    <input type='hidden' id='idTreinamentoModal'>
                  </div>
                </form>
              </section>
            </div>
          </div>
          <!-- Modal footer -->
          <div class="modal-footer">
            <button type="button" id="continuar" class="btn btn-primary">Continuar Inscrição</button>
          </div>
        </div>
      </div>
    </div>
</div>  

  <!-- OAuth -->
  <script type="text/javascript" src="/FluigTrainning/resources/js/oauth-1.0a.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/crypto-js.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/hmac-sha1.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/hmac-sha256.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/enc-base64.min.js"></script>
  <script type="text/javascript" src="/FluigTrainning/resources/js/DatasetFactoryOAuth.js"></script>


  <#--  JS  -->
  <script type="text/javascript" src="/webdesk/vcXMLRPC.js"></script>
  <script src="/FluigTrainning/resources/js/scrollJean.js"></script>
  <script src="/FluigTrainning/resources/js/FluigTrainning.js"></script>
  <script src="/FluigTrainning/resources/js/alertify.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>


</body>


</html>