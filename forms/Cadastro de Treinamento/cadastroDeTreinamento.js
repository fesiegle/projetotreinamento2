var editor = new Jodit('#descricaoCompleta', {
    removeButtons: ['image', 'upload'],
    height: 150

});
var table3;
var that = this;

$('#carregando').hide();

$('#myTab a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
});

$("#myModal").hide();

var treinamentos = [];
var that = this;


$("#salvar").click(function () {

    var validado = true;
    var _that = this;

    if ($('#titulo').val() == "" || $('#titulo').val() == undefined || $('#titulo').val() == null ||
        $('#instrutor').val() == "" || $('#instrutor').val() == undefined || $('#instrutor').val() == null ||
        $('#dataTreinamento').val() == "" || $('#dataTreinamento').val() == undefined || $('#dataTreinamento').val() == null ||
        $('#horario').val() == "" || $('#horario').val() == undefined || $('#horario').val() == null ||
        $('#quorum').val() == "" || $('#quorum').val() == undefined || $('#quorum').val() == null ||
        $('#nVagas').val() == "" || $('#nVagas').val() == undefined || $('#nVagas').val() == null ||
        $('#valor').val() == "" || $('#valor').val() == undefined || $('#valor').val() == null ||
        $('#descricaoResumida').val() == "" || $('#descricaoResumida').val() == undefined || $('#descricaoResumida').val() == null ||
        that.editor.value == "" || that.editor.value == undefined || that.editor.value == null
    ) {
        alert("Verifique se todos os campos foram prenchidos corretamente!");
        validado = false;
    }

    var input = $('#dataTreinamento').val().split('/');
    var hoje = moment().format('DD/MM/YYYY').split('/');
    var msg = "";
    var verdade = true;

    input[0] = parseInt(input[0]);
    input[1] = parseInt(input[1]);
    input[2] = parseInt(input[2]);

    hoje[0] = parseInt(hoje[0]);
    hoje[1] = parseInt(hoje[1]);
    hoje[2] = parseInt(hoje[2]);


    if (input[2] < hoje[2]) {
        msg += "Ano inválido";
        msg += " "
        verdade = false;

    }

    if (input[1] < hoje[1]) {
        msg += "Mês inválido";
        msg += " "
        verdade = false;
    }

    if (input[1] == hoje[1]) {

        if (input[0] < hoje[0]) {
            msg += "Dia inválido";
            msg += " "
            verdade = false;
        }

    }

    if (verdade == false) {
        alert(msg);
        validado = false;
    }


    if (validado == true) {
        var _xml;

        //Template envelope XML 
        $.ajax({
            url: './xml/createCard.xml',
            async: false,
            type: 'GET',
            datatype: 'xml',
            success: function (xml) {
                _xml = $(xml)
                console.log('PASSOU!')
            },
            fail: function (error) {}
        });

        //Alterar os valores recuperados na variavel _xml
        _xml.find('companyId').text('1');
        _xml.find('username').text('luis.siegle@totvs.com.br');
        _xml.find('password').text('!fefemito@17');
        _xml.find('parentDocumentId').text('65781');
        _xml.find('[name=titulo]').text($('#titulo').val());
        _xml.find('[name=instrutor]').text($('#instrutor').val());
        _xml.find('[name=dataTreinamento]').text($('#dataTreinamento').val());
        _xml.find('[name=horario]').text($('#horario').val());
        _xml.find('[name=unidade]').text($('#unidade').val());
        _xml.find('[name=nVagas]').text($('#nVagas').val());
        _xml.find('[name=quorum]').text($('#quorum').val());
        _xml.find('[name=valor]').text($('#valor').val());
        _xml.find('[name=status]').text($('#status').val());
        _xml.find('[name=descricaoResumida]').text($('#descricaoResumida').val());
        _xml.find('[name=descricaoCompleta]').text(that.editor.value);


        //Usar o metodo WCMAPI.Create para chamar o webservice
        parent.WCMAPI.Create({
            url: "/webdesk/ECMCardService?wsdl",
            contentType: "text/xml;charset=utf-8",
            dataType: "xml",
            data: _xml[0],
            success: function (data) {
                $(window).scrollTop(1);
                $(parent.window).scrollTop(1);
                FLUIGC.toast({
                    title: 'Aviso: ',
                    message: 'Cadastro Registrado!',
                    type: 'success',
                    timeout: 2000
                });
                setTimeout(function(){
                    location.reload(); 
                  }, 2000);
              




            }
        })
    }
});











function popula() {

    var ultimoRegistro = DatasetFactory.createConstraint("metadata#active", "true", "true", ConstraintType.MUST)
    var ds = DatasetFactory.getDataset('RegistroCadastroTrainning', null, [ultimoRegistro], null).values;


    for (var i = 0; i < ds.length; i++) {
        var data = ds[i]['dataTreinamento'];
        var titulo = ds[i]['titulo'];
        var instrutor = ds[i]['instrutor'];
        var nVagas = ds[i]['nVagas'];
        var valor = ds[i]['valor'];
        var horario = ds[i]['horario'];
        var quorum = ds[i]['quorum'];
        var unidade = ds[i]['unidade'];
        var status = ds[i]['status'];
        var id = ds[i]['documentid'];


        var filtroInscritos = DatasetFactory.createConstraint("treinamentoEscolhido", id, id, ConstraintType.MUST);
        var puxaInscritos = DatasetFactory.getDataset('registroInscritosTreinamentos', null, [filtroInscritos], null).values;

        var contagemInscritos = puxaInscritos.length;

        treinamentos.push([{
            id,
            data,
            titulo,
            instrutor,
            nVagas,
            valor,
            horario,
            quorum,
            unidade,
            status,
            contagemInscritos
        }])

    }
    console.log(treinamentos);
}

function mostra() {
    $.each(treinamentos, function (key, value) {
        $(".aqui").append("<tr class='treinamento'><td class='id' style='font-weight:bold;'>" + treinamentos[key][0].id + "</td><td class='titulo'>" + treinamentos[key][0].titulo + "</td><td class='status'>" + treinamentos[key][0].status + "</td><td class='data'>" + treinamentos[key][0].data + "</td><td class='horario'>" + treinamentos[key][0].horario + "</td><td class='unidade'>" + treinamentos[key][0].unidade + "</td><td class='instrutor'>" + treinamentos[key][0].instrutor + "</td><td class='nVagas'><span style='color:blue;' class='inscritosTreina'>" + treinamentos[key][0].contagemInscritos + "</span><span class='nVagas' style='color:blue;'>/" + treinamentos[key][0].nVagas + "</span></td><td class='quorum' style='color:red;'>" + treinamentos[key][0].quorum + "</td><td class='valor'>R$ " + treinamentos[key][0].valor + "</td></tr>");
    });
}




popula();
mostra();


var calend = FLUIGC.calendar('#dataTreinamento');

var table = $('#example').DataTable({
    select: true,
    dom: 'Bfrtip',
    buttons: [
        'excel', 'pdf'
    ],
    "language": {
        "sProcessing": "Processando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "Nenhum resultado encontrado",
        "sEmptyTable": "Nenhum dado na tabela",
        "sInfo": "",
        "sInfoEmpty": "",
        "sInfoFiltered": "(filtrado de um total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Carregando...",
        "sSelectItem": "",
        "select": {
            "rows": ""
        },

        "oPaginate": {
            "sFirst": "Primeiro",
            "sLast": "Ultimo",
            "sNext": "Seguinte",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }
});


$("#example_wrapper").prepend("<label>Relatório: </label><br>");



var buscaConfirmado=[];


var confirmados = DatasetFactory.getDataset('treinamentosConfirmados', null, null, null).values;
var listaTreinamentos =  DatasetFactory.getDataset('RegistroCadastroTrainning', null, null, null).values;

for (i=0;i<listaTreinamentos.length;i++){
  if(confirmados[i].idTreina==listaTreinamentos[i].documentid){
      buscaConfirmado.push(listaTreinamentos[i]);
  }
}





$.each(buscaConfirmado, function (key, value) {


    var input = buscaConfirmado[key].dataTreinamento.split('/');
    var hoje = moment().format('DD/MM/YYYY').split('/');
    var verdade = true;

    input[0] = parseInt(input[0]);
    input[1] = parseInt(input[1]);
    input[2] = parseInt(input[2]);

    hoje[0] = parseInt(hoje[0]);
    hoje[1] = parseInt(hoje[1]);
    hoje[2] = parseInt(hoje[2]);


    if (input[2] < hoje[2]) {
        verdade = false;

    }

    if (input[1] < hoje[1]) {
        verdade = false;
    }

    if (input[1] == hoje[1]) {

        if (input[0] < hoje[0]) {
            verdade = false;
        }

    }

    if (verdade == true) {
        validado = true;
    }
	else {

	    validado = false;
	}



    if(validado == true){
        $(".confirmadosAqui").append("<tr style='font-weight:bold;' class='treinamento'><td class='id' style='font-weight:bold;'>" + buscaConfirmado[key].documentid + "</td><td class='titulo'>" + buscaConfirmado[key].titulo + "</td><td class='status'>" + buscaConfirmado[key].status + "</td><td class='data'>" + buscaConfirmado[key].dataTreinamento + "</td><td class='horario'>" + buscaConfirmado[key].horario + "</td><td class='unidade'>" + buscaConfirmado[key].unidade + "</td><td class='instrutor'>" + buscaConfirmado[key].instrutor + "</td><td class='valor'>R$ " + buscaConfirmado[key].valor + "</td></tr>");
         }

    else{
        $(".confirmadosAqui2").append("<tr style='font-weight:bold;' class='treinamento'><td class='id' style='font-weight:bold;'>" + buscaConfirmado[key].documentid + "</td><td class='titulo'>" + buscaConfirmado[key].titulo + "</td><td class='status'>" + buscaConfirmado[key].status + "</td><td class='data'>" + buscaConfirmado[key].dataTreinamento + "</td><td class='horario'>" + buscaConfirmado[key].horario + "</td><td class='unidade'>" + buscaConfirmado[key].unidade + "</td><td class='instrutor'>" + buscaConfirmado[key].instrutor + "</td><td class='valor'>R$ " + buscaConfirmado[key].valor + "</td></tr>");
    }

 });


 

 var table4 = $('#acontecerConfirmados').DataTable({

    "paging": true,
    dom: 'Bfrtip',
    buttons: [
        'excel', 'pdf'
    ],
    "language": {
        "sProcessing": "Processando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "Nenhum resultado encontrado",
        "sEmptyTable": "Nenhum dado na tabela",
        "sInfo": "",
        "sInfoEmpty": "",
        "sInfoFiltered": "(filtrado de um total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Carregando...",
        "sSelectItem": "",
        "select": {
            "rows": ""
        },

        "oPaginate": {
            "sFirst": "Primeiro",
            "sLast": "Ultimo",
            "sNext": "Seguinte",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }

});

$("#acontecerConfirmados_wrapper").prepend("<label>Relatório: </label><br>");

var table5 = $('#passouConfirmados').DataTable({

    "paging": false,
    dom: 'Bfrtip',
    buttons: [
        'excel', 'pdf'
    ],
    "language": {
        "sProcessing": "Processando...",
        "sLengthMenu": "Mostrar _MENU_ registros",
        "sZeroRecords": "Nenhum resultado encontrado",
        "sEmptyTable": "Nenhum dado na tabela",
        "sInfo": "",
        "sInfoEmpty": "",
        "sInfoFiltered": "(filtrado de um total de _MAX_ registros)",
        "sInfoPostFix": "",
        "sSearch": "Buscar:",
        "sUrl": "",
        "sInfoThousands": ",",
        "sLoadingRecords": "Carregando...",
        "sSelectItem": "",
        "select": {
            "rows": ""
        },

        "oPaginate": {
            "sFirst": "Primeiro",
            "sLast": "Ultimo",
            "sNext": "Seguinte",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    }

});

$("#passouConfirmados_wrapper").prepend("<label>Relatório: </label><br>");


$('#editarCurso').click(function () {

    var filtroTreinamentoEdita = DatasetFactory.createConstraint("titulo", $(".selected").find('.titulo').html(), $(".selected").find('.titulo').html(), ConstraintType.MUST);
    var treinamentoEdita = DatasetFactory.getDataset('RegistroCadastroTrainning', null, [filtroTreinamentoEdita], null).values;
    var filtroTreinamento = DatasetFactory.createConstraint("treinamentoEscolhido", $(".selected").find('.titulo').html(), $(".selected").find('.titulo').html(), ConstraintType.MUST);
    var inscritosTreinamentos = DatasetFactory.getDataset('registroInscritosTreinamentos', null, [filtroTreinamento], null).values;


    $('#titulo').val(treinamentoEdita[0].titulo);
    window["instrutor"].setValue(treinamentoEdita[0].instrutor);
    $('#dataTreinamento').val(treinamentoEdita[0].dataTreinamento);
    $('#horario').val(treinamentoEdita[0].horario);
    $('#nVagas').val(treinamentoEdita[0].nVagas);
    $('#quorum').val(treinamentoEdita[0].quorum);
    $('#valor').val(treinamentoEdita[0].valor);
    $('#unidade').val(treinamentoEdita[0].unidade);
    $('#status').val(treinamentoEdita[0].status);
    $('#descricaoResumida').val(treinamentoEdita[0].descricaoResumida);
    that.editor.value = treinamentoEdita[0].descricaoCompleta;
    $('#documentId').val(treinamentoEdita[0].documentid);
    $('#salvar').hide();
    $("#mudancaCadastro").append("<button id='realizarAlteracao' class='btn btn-primary'>Realizar Alteração</button>");
    $('.nav-tabs a[href="#cadastro"]').tab('show');


    $("#realizarAlteracao").click(function () {
        var _xml;
        $.ajax({
            url: './xml/updateCardData.xml',
            async: false,
            type: 'GET',
            datatype: 'xml',
            success: function (xml) {
                _xml = $(xml)
                console.log('PASSOU!')
            },
            fail: function (error) {}
        });

        _xml.find('companyId').text('1');
        _xml.find('username').text('luis.siegle@totvs.com.br');
        _xml.find('password').text('!fefemito@17');
        _xml.find('cardId').text($('#documentId').val());
        _xml.find('[name=valor]').text($('#valor').val());
        _xml.find('[name=instrutor]').text($('#instrutor').val());
        _xml.find('[name=status]').text($('#status').val());
        _xml.find('[name=titulo]').text($('#titulo').val());
        _xml.find('[name=dataTreinamento]').text($('#dataTreinamento').val());
        _xml.find('[name=horario]').text($('#horario').val());
        _xml.find('[name=unidade]').text($('#unidade').val());
        _xml.find('[name=quorum]').text($('#quorum').val());
        _xml.find('[name=nVagas]').text($('#nVagas').val());
        _xml.find('[name=descricaoCompleta]').text(that.editor.value);
        _xml.find('[name=descricaoResumida]').text($('#descricaoResumida').val());

        parent.WCMAPI.Create({
            url: "/webdesk/ECMCardService?wsdl",
            contentType: "text/xml;charset=utf-8",
            dataType: "xml",
            data: _xml[0],
            success: function (data) {
                $(window).scrollTop(1);
                $(parent.window).scrollTop(1);
                FLUIGC.toast({
                    title: 'Aviso: ',
                    message: 'Cadastro Alterado!',
                    type: 'success',
                    timeout: 5000
                });
                console.log(_xml);
                
                setTimeout(function(){
                    location.reload(); 
                  }, 2000);
            
            
            }
            
        })

    });

    $(".nav-tabs li").click(function () {
        $("#realizarAlteracao").remove();
        $("#salvar").show();
        $('form').trigger("reset");
        window["instrutor"].clear();
        that.editor.value = "";
    })
});


$('#confirmar').click(function () {



    $('#carregando').show();

    var ciente = false;

    if($('#ciente').is(':checked')) {
        ciente=true;
     }
  
    if (ciente==true){

  
    var id = $(".selected").find('.id').html();
    var filtroInscritos = DatasetFactory.createConstraint("treinamentoEscolhido", id, id, ConstraintType.MUST);
    var puxaInscritosConfirmado = DatasetFactory.getDataset('registroInscritosTreinamentos', null, [filtroInscritos], null).values;


    var jaConfirmado=false;

    //tratar se for nulo
    var buscaConfirmados = DatasetFactory.getDataset('treinamentosConfirmados', null, null, null).values;
    

  
    $.each(buscaConfirmados, function (key) {
        if(buscaConfirmados[key].idTreina==id){
            jaConfirmado=true;
        }   
    });

    if(jaConfirmado==true){
        $('#carregando').hide();
        alert("Treinamento já confirmado!");
     
    
    }


    if(jaConfirmado==false){


    var tituloTreinamento = $(".selected").find('.titulo').html();
    var dataTreinamento = $(".selected").find('.data').html();
    var unidade = $(".selected").find('.unidade').html();
    var horario = $(".selected").find('.horario').html();
    var status = $(".selected").find('.status').html();
    var inscritos = $(".selected").find('.inscritostreina').html();

    var quorum = $(".selected").find('.quorum').html();
    var valor = $(".selected").find('.valor').html();

    if(inscritos>=quorum && status=="Ativo"){

             //Template envelope XML
           $.ajax({
            url: './xml/createCard.xml',
            async: false,
            type: 'GET',
            datatype: 'xml',
            success: function (xml) {
                _xml = $(xml)
                console.log('PASSOU!')
            },
            fail: function (error) {}
        });

        //Alterar os valores recuperados na variavel _xml
        _xml.find('companyId').text('1');
        _xml.find('username').text('luis.siegle@totvs.com.br');
        _xml.find('password').text('!fefemito@17');
        _xml.find('parentDocumentId').text('66710');
        _xml.find('[name=idTreina]').text(id);


        parent.WCMAPI.Create({
            url: "/webdesk/ECMCardService?wsdl",
            contentType: "text/xml;charset=utf-8",
            dataType: "xml",
            data: _xml[0],
            success: function (data) {
                //enviar email

                var nomes;
                var aqui=this;

                //para cada each executo a função
                $.each(puxaInscritosConfirmado, function (key) {
                    DatasetFactoryAuth.postSendEmail(puxaInscritosConfirmado[key].email, 'totvsrs-noreply@totvsrs.com.br', 'Confirmação de Inscrição para o Treinamento', 'templateConfTreinamentos', puxaInscritosConfirmado[key].nome, tituloTreinamento, valor,dataTreinamento, unidade);
                });

                //enviar email outro jeito
                // //populo uma variavel com os emails e executo a função apenas uma vez
                // $.each(puxaInscritosConfirmado, function (key) {
                //     nomes+= ""+puxaInscritosConfirmado[key].email+";"
                //    });

                // DatasetFactoryAuth.postSendEmail(nomes, 'totvsrs-noreply@totvsrs.com.br', 'Confirmação de Inscrição para o Treinamento', 'templateConfTreinamentos', puxaInscritosConfirmado[key].nome, tituloTreinamento, valor,dataTreinamento, unidade);
        
                $('#carregando').hide();

                $(window).scrollTop(1);
                $(parent.window).scrollTop(1);

                FLUIGC.toast({
                    title: 'Aviso: ',
                    message: 'Treinamento Confirmado!',
                    type: 'success',
                    timeout: 2000
                });
                setTimeout(function(){
                  location.reload(); 
                }, 2000);

            }
        })


    }

    else{
        $('#carregando').hide();
        alert("Impossivel confirmar treinamento, verifique se ele esta Ativo, verifique se o número de Inscritos não bateu com o Quorum")
       
    }

    }
    }
    else{
        $('#carregando').hide();
        alert("É necessário confirmar se esta ciente!")

    }
});






$('#verInscritos').click(function () {


    $('#tabelaInscritos').remove();
    $('.nav-tabs a[href="#inscritos"]').tab('show');


    var tituloTreinamento = $(".selected").find('.titulo').html();

    $('#selecionaTreinamento').val(tituloTreinamento);
    var totalQuorum = $(".selected").find('.quorum').html();
    var status = $(".selected").find('.status').html();
    var filtroTreinamento = DatasetFactory.createConstraint("treinamentoEscolhido", $(".selected").find('.id').html(), $(".selected").find('.id').html(), ConstraintType.MUST);
    var inscritosTreinamentos = DatasetFactory.getDataset('registroInscritosTreinamentos', null, [filtroTreinamento], null).values;
    $("#inscritos").append("<br><section id='tabelaInscritos'><h1>Inscritos no Treinamento " + tituloTreinamento + "</h1><br><span style='font-size:17px; font-weight: 700;'>Status: </span><span style='font-size:17px; font-weight: 700; color:blue;' id='statusInsc'>" + status + "</span><br><span style='font-size:17px; font-weight: 700;'>Total de Inscritos: </span><span style='font-size:17px; font-weight: 700; color:green;' id='totalInscritos'></span><br><span style='font-size:17px; font-weight: 700;'>Quórum: </span><span style='font-size:17px; font-weight: 700; color:red;' id='totalQuorum'>" + totalQuorum + "</span><br><br><button class='btn btn-primary pull-right' style='margin-bottom:20px;' id='alteraPagante'>Alterar Pagante</button><div id='content'><table id='example2' class='table'><thead><tr><th scope='col'>ID</th><th scope='col'>Nome</th> <th scope='col'>CNPJ</th><th scope='col'>RG</th><th scope='col'>Email</th><th scope='col'>Sexo</th><th scope='col'>Idade</th><th scope='col'>Telefone</th><th scope='col'>Celular</th><th scope='col'>Pago</th><th scope='col'>Selec.</td></tr></thead><tbody id='corpoInscritos'></tbody></table></table></div></section>");

    $.each(inscritosTreinamentos, function (key, value) {
        $("#corpoInscritos").append("<tr class='rowInscritos'><td style='font-weight:bold;' class='idInscrito'>" + inscritosTreinamentos[key].documentid + "</td><td class='nome'>" + inscritosTreinamentos[key].nome + "</td><td class='cnpj' style=''>" + inscritosTreinamentos[key].cnpj2 + "</td><td class='rg'>" + inscritosTreinamentos[key].rg + "</td><td class='email'>" + inscritosTreinamentos[key].email + "</tdi><td class='sexo'>" + inscritosTreinamentos[key].sexo + "</td><td class='Idade'>" + inscritosTreinamentos[key].dataNasc + "</td><td class='telefone'>" + inscritosTreinamentos[key].telefone + "</td><td class='celular'>" + inscritosTreinamentos[key].celular + "</td><td class='pagamentoInscrito'>" + inscritosTreinamentos[key].pagamentoInscrito + "</td><td> <input type='checkbox' class='selecao' name='selecionado'></td></tr>");

    });

    $('#totalInscritos').html(inscritosTreinamentos.length);


    var table2 = $('#example2').DataTable({

        "paging": false,
        dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf'
        ],
        "language": {
            "sProcessing": "Processando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "Nenhum resultado encontrado",
            "sEmptyTable": "Nenhum dado na tabela",
            "sInfo": "",
            "sInfoEmpty": "",
            "sInfoFiltered": "(filtrado de um total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Carregando...",
            "sSelectItem": "",
            "select": {
                "rows": ""
            },

            "oPaginate": {
                "sFirst": "Primeiro",
                "sLast": "Ultimo",
                "sNext": "Seguinte",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }

    });

    $("#example2_wrapper").prepend("<label>Relatório: </label><br>");

    $("#example2_filter").css('text-align','start');
    $("#example2_filter input").addClass('form-control');

    $('#alteraPagante').click(function () {

        var checados = [];
        var that = this;

        $(':checkbox:checked').each(function () {
            var message = "";
            var row = $(this).closest("tr")[0];
            var id = row.cells[0].innerHTML;
            var status = row.cells[9].innerHTML;
            checados.push([{
                id,
                status
            }]);
        });

        $.each(checados, function (key) {
            var _xml;
            $.ajax({
                url: './xml/updateCardData.xml',
                async: false,
                type: 'GET',
                datatype: 'xml',
                success: function (xml) {
                    _xml = $(xml)
                },
                fail: function (error) {}
            });

            _xml.find('companyId').text('1');
            _xml.find('username').text('luis.siegle@totvs.com.br');
            _xml.find('password').text('!fefemito@17');
            _xml.find('cardId').text(checados[key][0].id);

            if (checados[key][0].status == "Não") {

                _xml.find('[name=pagamentoInscrito]').text('Sim');
            } else {
                _xml.find('[name=pagamentoInscrito]').text('Não');
            }

            parent.WCMAPI.Create({
                url: "/webdesk/ECMCardService?wsdl",
                contentType: "text/xml;charset=utf-8",
                dataType: "xml",
                data: _xml[0],
                success: function (data) {
                    console.log(_xml);
                }
            })

        });

        $(window).scrollTop(1);
        $(parent.window).scrollTop(1);

        FLUIGC.toast({
            title: 'Aviso: ',
            message: 'Pagamento Alterado com Sucesso!',
            type: 'success',
            timeout: 5000
        });
        setTimeout(function(){
            location.reload(); 
          }, 2000);
      
    });


    alteraBotoes();

});



function populaInscritos() {

    var filtroTreinamento = DatasetFactory.createConstraint("titulo", $('#selecionaTreinamento').val(), $('#selecionaTreinamento').val(), ConstraintType.MUST);
    var treinamentoSelect = DatasetFactory.getDataset('RegistroCadastroTrainning', null, [filtroTreinamento], null).values;
    var filtroInscritos = DatasetFactory.createConstraint("treinamentoEscolhido", treinamentoSelect[0].documentid, treinamentoSelect[0].documentid, ConstraintType.MUST);
    var inscritosTreinamentos = DatasetFactory.getDataset('registroInscritosTreinamentos', null, [filtroInscritos], null).values;

    
    $("#inscritos").append("<section id='tabelaInscritos'><h1>Inscritos no Treinamento " + treinamentoSelect[0].titulo + "</h1><span style='font-size:17px; font-weight: 700;'>Quórum: </span><span style='font-size:17px; font-weight: 700; color:blue;' id='statusInsc'>" + treinamentoSelect[0].status + "</span><br><span style='font-size:17px; font-weight: 700;'>Total de Inscritos: </span><span style='font-size:17px; font-weight: 700; color:green;' id='totalInscritos'></span><br><span style='font-size:17px; font-weight: 700;'>Quórum: </span><span style='font-size:17px; font-weight: 700; color:red;' id='totalQuorum'>" + treinamentoSelect[0].quorum + "</span><br><br><button class='btn btn-primary pull-right' style='margin-bottom:20px;' id='alteraPagante'>Alterar Pagante</button><br><br><hr><div id='content'><table id='example3' class='table'><thead><tr><th scope='col'>ID</th><th scope='col'>Nome</th> <th scope='col'>CNPJ</th><th scope='col'>RG</th><th scope='col'>Email</th><th scope='col'>Sexo</th><th scope='col'>Idade</th><th scope='col'>Telefone</th><th scope='col'>Celular</th><th scope='col'>Pago</th><th scope='col'>Selec.</td></tr></thead><tbody id='corpoInscritos'></tbody></table></table></div></section>");

    $.each(inscritosTreinamentos, function (key) {
    
      
        $("#corpoInscritos").append("<tr><td style='font-weight:bold;' class='idInscrito'>" + inscritosTreinamentos[key].documentid + "</td><td class='nome'>" + inscritosTreinamentos[key].nome + "</td><td class='cnpj' style=''>" + inscritosTreinamentos[key].cnpj2 + "</td><td class='rg'>" + inscritosTreinamentos[key].rg + "</td><td class='email'>" + inscritosTreinamentos[key].email + "</tdi><td class='sexo'>" + inscritosTreinamentos[key].sexo + "</td><td class='Idade'>" + inscritosTreinamentos[key].dataNasc + "</td><td class='telefone'>" + inscritosTreinamentos[key].telefone + "</td><td class='celular'>" + inscritosTreinamentos[key].celular + "</td><td class='pagamentoInscrito'>" + inscritosTreinamentos[key].pagamentoInscrito + "</td><td> <input type='checkbox' class='selecao' name='selecionado'></td></tr>");

        if ($('.pagamentoInscrito').eq(key).html() == "Não") {
            $('.pagamentoInscrito').eq(key).css("color", "red");
        } else {
            $('.pagamentoInscrito').eq(key).css("color", "green");
        };
    });

    $('#totalInscritos').html(inscritosTreinamentos.length);


    that.table3 = $('#example3').DataTable({

        "paging": false,
        dom: 'Bfrtip',
        buttons: [
            'excel', 'pdf'
        ],
        "language": {
            "sProcessing": "Processando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "Nenhum resultado encontrado",
            "sEmptyTable": "Nenhum dado na tabela",
            "sInfo": "",
            "sInfoEmpty": "",
            "sInfoFiltered": "(filtrado de um total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Carregando...",
            "sSelectItem": "",
            "select": {
                "rows": ""
            },

            "oPaginate": {
                "sFirst": "Primeiro",
                "sLast": "Ultimo",
                "sNext": "Seguinte",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }

    });

    $("#example3_wrapper").prepend("<label>Relatório: </label><br>");

    $('#alteraPagante').click(function () {

        var checados = [];
        var that = this;

        $(':checkbox:checked').each(function () {
            var message = "";
            var row = $(this).closest("tr")[0];
            var id = row.cells[0].innerHTML;
            var status = row.cells[9].innerHTML;
            checados.push([{
                id,
                status
            }]);
        });

        $.each(checados, function (key) {
            var _xml;
            $.ajax({
                url: './xml/updateCardData.xml',
                async: false,
                type: 'GET',
                datatype: 'xml',
                success: function (xml) {
                    _xml = $(xml)
                },
                fail: function (error) {}
            });

            _xml.find('companyId').text('1');
            _xml.find('username').text('luis.siegle@totvs.com.br');
            _xml.find('password').text('!fefemito@17');
            _xml.find('cardId').text(checados[key][0].id);

            if (checados[key][0].status == "Não") {

                _xml.find('[name=pagamentoInscrito]').text('Sim');
            } else {
                _xml.find('[name=pagamentoInscrito]').text('Não');
            }

            parent.WCMAPI.Create({
                url: "/webdesk/ECMCardService?wsdl",
                contentType: "text/xml;charset=utf-8",
                dataType: "xml",
                data: _xml[0],
                success: function (data) {
                    console.log(_xml);
                }
            })

        });

        $(window).scrollTop(1);
        $(parent.window).scrollTop(1);

        FLUIGC.toast({
            title: 'Aviso: ',
            message: 'Pagamento Alterado com Sucesso!',
            type: 'success',
            timeout: 5000
        });
        setTimeout(function(){
            location.reload(); 
          }, 2000);
      
    });

    // $('.nav-tabs li').click(function () {
    //     // $('#confirmar').remove();
    //     $('#tabelaInscritos').remove();
    //     table2.destroy();


    // });


    //talvez nao remover tabela apenas os dados dela...
    // 

}

$("#selecionaTreinamento").change(function () {
    that.table3.destroy();
    $('#tabelaInscritos').remove();
    populaInscritos();
});


populaInscritos();





function alteraBotoes() {
    $(".buttons-excel").removeClass('dt-button');
    $(".buttons-pdf").removeClass('dt-button');
    $(".buttons-excel").addClass('btn btn-default');
    $(".buttons-pdf").addClass('btn btn-default');

    $(".buttons-excel").css('box-shadow', 'none');
    $(".buttons-excel").css('border', '2px solid green');
    $(".buttons-excel").css('color', 'green');
    $(".buttons-excel").css('font-weight', 'bold');


    $(".buttons-pdf").css('box-shadow', 'none');
    $(".buttons-pdf").css('border', '2px solid #c00');
    $(".buttons-pdf").css('font-weight', 'bold');
    $(".buttons-pdf").css('color', '#c00');
}

alteraBotoes();

$("#example_filter").css('text-align','start');
$("#example_filter input").addClass('form-control');
$("#example3_filter").css('text-align','start');
$("#example3_filter input").addClass('form-control');
$("#acontecerConfirmados_filter").css('text-align','start');
$("#acontecerConfirmados_filter input").addClass('form-control');
$("#passouConfirmados_filter").css('text-align','start');
$("#passouConfirmados_filter input").addClass('form-control');


